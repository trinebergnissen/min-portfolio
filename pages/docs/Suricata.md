# Suricata

opnsense --> det er en open source router - som vi så har installeret Suricata på

## Opgave 28 - IDS/IPS Regler

### **Information**

Suricata bruger regler til at detektere signaturer i netværkstrafik.

Regler er opbygget af 3 dele:

* action definerer hvad der skal ske når der er et match, f.eks alert eller drop

* header definerer retning, ip adresser/netværk, protokol og port

* rule options definerer yderligere 
konfiguration af en regel

Du har allerede lavet din første regel i opgave 27 - Suricata opsætning hvor du tilføjede denne regel til local.rules filen:

    alert icmp any any -> any any (msg:"ping/icmp detected"; sid:5000000;)      

Her er en forklaring af hvordan reglen er opbygget:

* alert er action

* icmp any any -> any any er header icmp er protokollen, any any -> any any betyder fra alle ip adresser på alle porte til alle ip adresser på alle porte. Her kunne være specificeret en specifik ip adresse/netværk og port i både afsender og modtager. Retningen -> kunne have været specificeret i begge retninger <>.

* (msg:"ping/icmp detected"; sid:5000000;) er rule options, msg er den tekst der skrives i fast.log og eve.json. sid skal angives og være unik for hver regel.

Det kan være en fordel at have pcap filen åben i wireshark samtidig med at du laver opgaven.

### **Instruktioner**

**1. Læs om regler i Suricata dokumentationen og dan dig et overblik over mulighederne**

.

**2. Lav en ny mappe på den vm hvor du har installeret Suricata**

    cd mkdir pcap-zip-files

**3. På suricata maskinen hent pcap filen fra https://www.malware-traffic-analysis.net/2018/07/15/2018-07-15-traffic-analysis-exercise.pcap.zip (brug wget, zip password er infected)**

wget "link"

unzip 

**4. Skriv en regel i /etc/suricata/rules/local.rules der laver en alert på udp trafik fra 10.0.0.201 port 63448 til 5.138.53.160 port 41230. Dokumenter hvor mange alerts du får og hvad output fra de relevante alerts er i fast.log ?**

    alert udp 10.0.0.201 63448 -> 5.138.53.160 41230 (msg:"udp detected on specified ip and port"; sid:5000001;)

**5. Skriv en regel der alerter på http trafik fra alle ip og porte til alle ip og porte hvor http content type er image/jpeg. Dokumenter hvor mange alerts du får og hvad output fra de relevante alerts er i fast.log ?**

.

**6. Skriv en regel der alerter på http trafik fra alle ip og porte til alle ip og porte hvor http content type er application/x-bittorrent**

    alert http any any <> any any (msg:"http trafic detected"; sid:5000002;)

**7. Skriv en regel der alerter på http trafik fra alle ip og porte til alle ip og porte hvor ordet Betty_Boop indgår. Dokumenter hvor mange alerts du ser, samt hvilke ip adresser og porte der er involveret i kommunikationen ?**

.

## Opgave 29 - Suricata live

**Netværksdiagram over netværket**

![Alt text](img/Suricata_netv%C3%A6rksdiagram.png)

