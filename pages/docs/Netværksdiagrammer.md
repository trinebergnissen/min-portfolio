# Netværksdiagrammer

## Fysisk diagram

Viser hvordan ting er kablet.
(Viser ingen ip-adresser)

VLAN på det fysiske diagram svarer til IP-adresserne på subnettene på det logiske diagram.

**Fysisk diagram (Det mest rigtigt - med de rigtige symboler)**

![Alt text](img/Netv%C3%A6rksdiagram_fysisk_2.png)

**Fysisk diagram (Næmmere læseligt for at få en forståelse for netværk)**

![Alt text](img/Netv%C3%A6rksdiagram_fysisk_1.png)


## Logisk diagram

Viser forbindelser -> kan være trådløse forbindelser. 
Viser desuden IP-adresser.

**Logisk diagram (Det mest rigtige - med de rigtige symboler)**

![Alt text](img/Netv%C3%A6rksdiagram_logisk_2.png)

**Logisk diagram (Næmmere læseligt for at få en forståelse for netværk)**

![Alt text](img/Netv%C3%A6rksdiagram_logisk_1.png)


