# Fysiske trussler

## Identificer hvad de forskellige trussler kompromitere i forhold til CIA

1. Computeren bliver stjålet
* Tilgængelighed (Avalability)

2. Computeren bliver ødelagt
* Tilgængelighed og integritet (Avalability og integrati)

3. Der opnås fysisk adgang til computeren
* Fortrolighed og integritet (Confidentiality og integrati)


## DROP vs. RECEJT på firewall

DROP -> så smider den pakken uden at returnere noget 

REJECT -> så afviser den pakker, og fortæller at pakken er blevet afvist
