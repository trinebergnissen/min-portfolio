# Logging i Linux

## /var/run/utmp (who) - viser alle nuværende indloggede brugere

![Alt text](img/logs_var_run_utmp.png)

## /var/log/wtmp (last)

![Alt text](img/logs_var_log_wtmp.png)

## /var/log/lastlog (lastlog)

![Alt text](img/logs_var_log_lastlog.png)

## /var/log/btmp (lasb) - viser fejlede login forsøg

![Alt text](img/logs_var_log_btmp.png)

## su and ssh logins

![Alt text](img/logs_su_og_ssh.png)