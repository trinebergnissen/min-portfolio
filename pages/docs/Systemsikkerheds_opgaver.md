# Opgaver

## OS og Linux

### **Opgave 2: Reflekter over formålet med et operativ system og deres kontekst ift. it-sikkerhed, og noter overvejelserne ned**
Formålet med et operativ system er at det kontrollere eksekveringen af programmer på enheden. Og sørger bl.a. for at der kan kører flere programmer på enheden på samme tid. 

Det fungere som et mellemliggende lag mellem software og computer hardwaren.

OS styer kommunikationen mellem applikationer og computerens interne ressourcer.

Ovenpå OS kernen ligger et OS API som er det applikationerne snakker sammen med.

![Alt text](img/OS.png)

*Kontekst i forhold til it-sikkerhed:*

Får en hacker adgang til operativ systemet på en bruger med admin rettigheder, kan de gøre alt på computeren.

### **Opgave 3: Eksekver kommandoerne (I Ubuntu) og noter hvad de gør:**
**1. Eksekver kommandoen *"pwd"***

Viser stien/path'en. Viser hvilket directory man er i.

Her: /home/trine

**2. Eksekver kommandoen *"cd .."***

Går tilbage til home directory.

**3. Eksekver kommandoen *"cd /"***

Går tilbage til start/tilbage til udgangspunktet

**4. Lokationen *"/"* har en bestemt betydning i Linux file systemet. Hvad er det?**

**5. Eksekver kommandoen *"cd /etc/ca-certificates/"***

Går til directoy "etc/ca-certificates/"

**6. Hvor mange Directories viser outputtet fra *"pwd"* kommandoen nu?**

2cd

**7. Eksekver kommandoen *"cd ../.."***

Går tilbage til "/$"

**8. Hvor mange Directories viser outputtet fra *"pwd"* kommandoen nu?**

Ingen - den viser kun "/"

**9. Eksekver kommandoen cd ~ (Karakteren hedder tilde. I Ubuntu kan den nogen gange findes med knappen F6)**

**10. Kommandoen ~ er en "Genvej" i linux, hvad er det en genvej til?**

Går tilbage til udgangspunktet: /home/trine

**11. I file systemets rod(/), esekver kommandoen ls**

Den lister alle filerne i systemets rod.

Her: bin, dev, home, lib32, libx32, media, opt, root, sbin, srv, tmp, var, boot, etc, lib, lib64, lost+found, mnt, proc, run, shap, sys, usr

**12. I brugerens Home directory, eksekver kommandoen touch helloworld.txt**

Får en "Permission denied"??

**13. I brugerens Home directory, eksekver kommandoen touch .helloworld.txt**

Får en "Permission denied"??

**14. List alle filer og mapper i brugerens Home directory**

trine

**15. List alle filer og mapper i brugerens Home directory med flaget -a**

. .. trine

**16. List alle filer og mapper i brugerens Home directory med flaget -l**

Output er:

total 4

drwxr-x--- 4 trine trine 4096 Feb  9 14:30 trine

**17. List alle filer og mapper i brugerens Home directory med flaget -la**

Outputtet er:

total 12

drwxr-xr-x 3  root  root  4096 Feb  9 14:17 .

drwxr-xr-x 19 root  root  4096 Feb  9 14:14 ..

drwxr-x--- 4  trine trine 4096 Feb  9 14:30 trine

**18. I brugerens Home directory, eksekver kommandoen mkdir helloWorld**

**19. Eksekver kommandoen ls -d */**

**20. Eksekver kommandoenn ls -f**


### **Opgave 4**

I file strukteren i Linux har hver directory pr. konvention et specifikt formål, altså hvad indeholder de. Undersøg og noter i linux cheats sheet hvad det er for nogle filer som er tiltænkt hver enkelt directory

/ -> står for roden af filsystemets træ

|_bin -> binære filer - indeholder de grundlæggende værktøjer som alle brugere har brug for

|_boot -> indeholder alle de filer som er nødvendige for computerens opstartsproces

|_dev -> indeholder filer til alle blok enheder/tilbehørsenheder (harddisk)

|_etc -> indeholder konfigurations filer til systemet

|_home -> er brugerens hjemmemappe - indeholder startbibliotekter til brugeren -> det er her brugerens egne personlige filer skal opbevares

|_media -> er til eksterne harddiske/udtagelige enheder - fx USB-drev og CD-drev

|_lib -> er systembibliotekerne og indeholder filer som kernemoduler og enhedsdrivere

|_mnt -> er til netværksdrev

|_misc -> bruges til diverse opgaver

|_proc -> indeholder virtuelle filsystemer, der beskriver procesoplynsingerne som filer

|_opt -> indeholder tilføjelsesoftware - større programmer kan installeres her i stedet for i /usr

|_sbin -> er systemets binære filer - indeholder de grundlæggende værktøjer, der er nødvendige for at starte, vedligeholde og gendanne computeren

|_root -> er standardplaceringen for computerens administrator-rod

|_tmp -> til midlertidige filer - disse fjernes normalt ved opstart

|_usr -> indeholder implementerbare og delte ressourcer, der ikke er systemkritiske

|_var -> variable filer, filer der ændrer sig ofte - det kan fx være logfiler og wwww filer


### **Opgave 5: Eksekver kommandoerne (I Ubuntu) og noter hvad de gør:**

**Eksekver kommandoen apt --help**

Den giver oplysninger om hvad "atp" er -> det er en commandline packets manager og har forskellige commandoer til fx at søge og manage pakker -> eksempelvis atp --list (??)

**via apt --help skal du nu finde ud af hvordan du udskriver en liste til konsolen, som viser alle installeret pakker.**

"apt list"?? -> udskriver en meget lang liste som bliver ved med at udskrive til konsolen læææænge og meget hurtigt

**Eksekver kommandoen apt -h**

"apt -h" -> er det samme som at skrive "apt --help" (se længere oppe)

**Eksekver kommandoen man apt og scroll ned i bunden af konsol outputtet(pgdwn)**

**Eksekver kommandoen man ls og scroll ned i bunden af konsol outputtet**

Lister forskellige kommandoer??

**Eksekver kommandoen man man og skim outputtet, hvilken information kan du finde der?**



**Eksekver kommandoen help**


### **Opgave 9**

**Opret en ny bruger - I denne opgave skal der oprettes en ny bruger ved navn Mandalorian**

"sudo useradd Mandalorian"

**Tildel en bruger password - I denne opgave skal brugeren Mandalorian, havde tildelt et password**

"sudo passwd Mandalorian"

**Ændring af eksisterende bruger konto opsætning - I denne opgave skal Mandalorian bruger kontoen ændres således den har en udløbs dato 23-06-2023**

"sudo usermod -e 2023-06-23 Mandalorian"

**Ændring af brugerens hjemme directory - I denne opgave skal bruger kontoen Mandalorian havde ændret sit hjemme directory til /home/mandoFiles/. husk at oprette directoriet først**

"mkdir Home"

"cd Home"

"mkdir mandoFiles"

"cd"

"sudo usermod -d /Home/mandoFiles Mandalorian"


**Opret tre filer med brugeren Mandalorian og find dem med find kommandoen, eksekver søgning fra /**

For at kunne oprette filer skal brugeren have sudo rettigheder - de tildeles med kommandoen:
"sudo adduser <UserNameHere> sudo"

Derefter oprettet 3 filer med kommandoen:
"sudo touch <filnavn>"

Derefter åbnet filerne en efter en og skrevet i dem med kommandoen:
"sudo nano <filnavn>"

Ejeren af filen står til at være "root" - hmm er det fordi brugeren er sudo bruger??


### **Opgave 12**

**Primærer log file**
Den primær log file for systemet hedder syslog, og indeholder information om stort alt systemet foretager sig.

**1. Udskriv inholdet af denne file**

Gjort med kommandoerne: 
    
"cd .." - indtil jeg var helt ude
    
"cd var"

"cd log"

"cat syslog"

**2. Studer log formattet, og skriv et "generisk" format ind i dit Linux cheat sheet**

timestamp - hostname - process [pid]: message

Eksempel:

Mar 16 13:23:11 server systemd[1258]: Startup finished in 327ms.

**3. Noter hvilken tids zone loggen bruger til tidsstemple, er det UTC?**

**Authentication log**
Log filen auth.log indeholder information om autentificering.

**1. Udskriv indholdet af auth.log**

Den har formattet:

timestamp - hostname - login[xxxx]: pam_unix(login:auth): message

**2. Skrift bruger til F.eks. root**

Done

**3. Udskriv indholdet af auth.log igen, og bemærk de nye rækker**

Der er tilføjet rækker, hvor login'et til root brugeren er blevet logget

**4. Skrift tilbage til din primær bruger igen (som naturligvis ikke er root)**

Done

**5. Udskriv indholdet af auth.log igen, og bemærk de nye rækker**

Der er tilføjet rækker, hvor login'et til min egen bruger et blevet logget


### **Opgave 13**

**Opsætning af locate til søgning**

Kommandoen "find" er god til søgning af filer, men "locate" kan også med fordel anvendes.

**1. Installer locate med kommandoen "sudo apt install locate"**

Done

**2. Opdater "Files on disk" databasen, "sudo updatedb"**

Done

**Dan overblik over Rsyslog logfilerne på operativ systemet**

**1. Brug locate til at finde alle filer med ordet rsyslog**

Kommando: "locate rsyslog"

**2. Dan dig et generelt overblik over filerne. Er der mange tilknyttet filer?**

Ja, der er mange filer.

**Rsyslog konfigurations file**

Rsyslog log konfiguration filen indeholder den generelle opsætningen af rsyslog daemon, blandt andet hvem der ejer log filerne, og hvilken gruppe der er tilknyttet log filerne. Herudover har den module opsætning. Moduler er ekstra funktionalitet som man kan give til rlogsys.

**1. Brug locate til at finde rsyslog filen rsyslog.conf**

Den ligger her: /etc/rsyslog.conf

**2. Åben filen med nano**

Kommando: "cd etc", "nano rsyslog.conf"

**3. I konfigurations filen, find afsnittet "Set the default permissions for all log files"**

Done

**4. Noter hvem der er file ejer, og hvilken gruppe log filerne er tilknyttet**

FileOwner -> syslog

FileGroup -> adm

**5. Udforsk de andre områder af filen**


## **Opgave 14**

Reglerne for hvad rsyslog skal logge, findes i filen 50-default.conf.

Reglerne for skrives op som facility.priority action

Facility er programmet der bliver logget fra F.eks. mail eller kern. Priority fortæller system hvilket types besked der skal logges. Beskeder typer identificerer hvilken priortering den enkelte log har.

**i rsyslog directory, skal filen "50-default.conf" findes**

Done

**Åben filen "50-deault.conf"**

Done

**Dan et overblik over alle de log filer som der bliver sendt beskeder til og noter hvilken filer mail applikationen sender log beskeder til, ved priorteringen info , warn og err**

Mail applikationen sender mails til følgende logfiler:

/var/log/mail.info

/var/log/mail.warn

/var/log/mail.err


## **Opgave 15**

I disse opgaver skal der arbejdes med log rotation i Linux (Ubuntu).

Du kan finde hjælp i Ubuntu man page til logrotate.

**Åben filen /etc/logrotate.conf**

**Sæt log rotationen til daglig rotation**

**Sæt antallet af backlogs til 8 uger.**


## **Opgave 16**

**Eksekver kommandoen "service rsyslog stop". Efter dette vil der ikke længere blive genereret logs i operativ systemet**

Done

**Eksekver kommandoen "service rsyslog start"**

Done

*Typisk er den kun superbrugerer som kan lukke ned for en log service. Det betyder at hvis en angriber kan lukke ned for logging servicen, kan han også lukke ned for evt. sikkerheds mekanismer som er i samme operativ system. Overvej hvordan man kan undgå dette? med F.eks. log overvågning via netværk.*


## **Opgave 17**

**1. Hvad hedder den rsyslog file som indeholder alle begivenheder som relaterer til autentificering?**

/var/log/auth.log

**2. Hvilken log file indeholder oplysninger om bruger der er logget ind på nuværende tidspunkt?**

**3. Hvilken log system eksisterer på næsten alle moderne Linux systemer?**


## **Opgave 19**

**1. Installer iptables med kommandoen "sudo apt install iptables"**

Installeret på ubuntu serveren i vmware.

**2. Udskriv version nummeret med kommandoen "sudo iptables -V"**

Den har version v1.8.7

**3. Installer iptables-persistent med kommandoen "sudo apt install iptables-persistent"**

Done

**4. Udskriv alle firewall regler(Regel kæden) med kommandoen "sudo iptables -L"**

Alle firewall regler udskrives med kommandoen:

"sudo iptables -L"

**5. Reflekter over hvad der menes med firewall regler, samt hvad der menes med "regel kæden". Når man har ændret en iptables firewall regel, skal man manuelt gemme den nuværende regel opsætning med kommandoen "sudo netfilter-persistent save" ellers er regel listen tom efter genstart**

Det hedder en regel kæde fordi firewallen behandler reglerne en af gangen i en kronologisk rækkefølge som de står i regel kæden. Regel 1 behandles altså før regel 2, som igen behandles før regel 3 osv.

## **Opgave 20**

**1. Udskriv regel kæden, og noter dig hvordan den ser ud. (Regel kæden er tidligere blevet udskrevet i opgave 19, trin 4)**

**Den indeholder:**

Chain INPUT (policy ACCEPT)

target     prot opt source      destination

Chain FORWARD (policy ACCEPT)

target     prot opt source      destination

Chain OUTPUT (policy ACCEPT)

target     prot opt soruce      destination


**2. Lav en regel i slutningen af regelkæden som dropper alt trafik med kommandoen "sudo iptables -A INPUT -j DROP"**

Kommando til at droppe alt trafikken:

"sudo iptables -A INPUT -j DROP"

**3. Udskriv regel kæden igen, og noter forskellen fra trin 1**

**Den indeholder:**

Chain INPUT (policy ACCEPT)

target     prot opt source      destination

*DROP       all -- anywere       anywere*

Chain FORWARD (policy ACCEPT)

target     prot opt source      destination

Chain OUTPUT (policy ACCEPT)

target     prot opt soruce      destination


## **Opgave 21**

**Information**
iptables er en "stateful" firewall. Det betyder at den blandt andet kan holde styr på hvilken forbindelser operativ systemet(klient) har oprettet til server. Dette skal vi arbejde med i den følgende opgave.

**Instruktioner**
**1. Eksekver kommandoen sudo iptables -I INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT**

*I kommandoen vælger vi at indsætte reglen først i regelen kæden med -I muligheden. Herefter vælges det er et iptables module med -m conntrack (conntrack==connection track) for at iptables kan holde styr på hvilken forbindelser der er etableret, og af hvem. Til sidst bruges muligheden --ctstate ESTABLISHED,RELATED -j ACCEPT som tillader indadgående pakker fra serverer hvor klienten(OS) selv har oprettet forbindelse.*

**2. Udskriv reglen listen**

**Den indeholder:**

Chain INPUT (policy ACCEPT)

target     prot opt source      destination

*ACCEPT     all -- anywere       anywere         cstate RELATED,ESTABLISHED*

DROP       all -- anywere       anywere

Chain FORWARD (policy ACCEPT)

target     prot opt source      destination

Chain OUTPUT (policy ACCEPT)

target     prot opt soruce      destination


## **Opgave 22**

**Information**
Loopback interfacet (lo,også kendt som 127.0.0.1 eller localhost) bruges til en række ting internt i Linux. Derfor kan det give mening at tillade indadgående trafik fra denne.

Udover at Linux bruger loopback til interne operationer, så er input fra loopback også en stor hjælp ift. evt. fejlfinding på applikationer.

**Instruktioner**

**Tillad forbindelser fra loopback interfacet med kommandoen sudo iptables -I INPUT 1 -i lo -j ACCEPT**

**Regel listen indeholder nu:**

Chain INPUT (policy ACCEPT)

target     prot opt source      destination

*ACCEPT     all -- anywere      anywere*

ACCEPT     all -- anywere       anywere         cstate RELATED,ESTABLISHED

DROP       all -- anywere       anywere

Chain FORWARD (policy ACCEPT)

target     prot opt source      destination

Chain OUTPUT (policy ACCEPT)

target     prot opt soruce      destination


