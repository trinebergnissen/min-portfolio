# Wazuh server

### **Hent OVA fil**

[https://packages.wazuh.com/4.x/vm/wazuh-4.4.1.ova](https://packages.wazuh.com/4.x/vm/wazuh-4.4.1.ova "OVA")

### **Sæt tastatur til dansk**

    localectl set-keymap dk    

### **Tjek om serveren kører**

    sudo systemctl status wazuh-manager

### **Log ud med kommandoen**

    logout
