# Netværks- og kommunikationssikkerheds opgaver

## **Opgave 2 - OSI modellen** 
### **2.1: Find almindelige (consumer) eksempler på hardware enheder, og placer dem på OSI modellen**

7 Applikations laget 
-> computer, mobiltelefon, tablet, tv, gateway

6 Præsentations laget
-> computer, mobiltelefon, tablet, tv, gateway

5 Sessions laget 
-> computer, mobiltelefon, tablet, tv, gateway

4 Transport laget 
-> firewall
-> hvordan de snakker sammen

3 Netværks laget 
-> router, firewall
-> logiske adresser (ip-adresser)
-> hvem der snakker sammen

2 Data Link laget 
-> switch, bridge, access point
-> når vi sender direkte fra en enhed til en anden
-> her har vi hardware adresser (MAC adresser??)

1 Det fysiske lag 
-> kabler, stiktype, wifi, bluetooth, modem, hub
 
### **2.2: Find nye/sjældne/esoteriske enheder (fysiske eller virtuelle) (med referencer) - f.eks. next-gen firewalls, proxies, application gateways. Hvad-som-helst med et netstik/wifi er ok at have med**
...



## **Opgave 3 - Fejlfinding**
### **3.1: Vælg et problem, gerne når det opstår, alternativt på bagkant. Det kunne være printerfejl, wifi connection issues, hjemmesider der er ‘væk’, langsomt internet, certifikat fejl, mv**
Problemer med opsætning af GitLab.

### **3.2: Beskriv problemet. Vær opmærksom på forskellen mellem hvad der er observeret, og hvad der er “rygmarvsreaktion”**
Der bliver ikke oploadet noget til GitLab ved "git add .", "git commit -m "kommentar", "git push".

### **3.3: Beskriv hvad du tror problemet er (ie. formuler en hypotese)**
Problemer med Git Runner -> Git Runner er ikke blevet installeret rigtigt.

### **3.4: Find oplysninger der underbygger/afkræfter ideen (ie. se i logfiler, lav tests, check lysdioder, …) og skriv det ned**
Snakket med medstuderende som havde fået det til at virke.
De brugte ikke Git Runner. 
Hypotese om at Git Runner var problemet blev derfor afkræftet.
Medstudernede havde til gengæld ændret i yaml filen for at få det til at virke.

### **3.5: Hvis ideen blev afkræftet, lav en ny hypotese**
Ny hypotese: Problemet skyldes fejl i yaml filen.

### **3.6: Beslut hvad der kan gøres for at afhjælpe eller mitigere fejlen, hvis den skulle komme igen**

### **3.7: Implementer det hvis ressourcerne tillader det, eller beskriv hvordan det skal håndteres til næste gang**
Foretog de nødvendige ændrigner i yaml filen. 
-> Noget af teksten skulles tabuleres en række ind.



## **Opgave 4 - Specs**
### **4.1 og 4.2: Find listen fra ww06, hvor lavede vi en liste over hardware og deres plads i OSI modellen, og udvælg 2-3 forskellige enheder fra listen**
* ASUS RT-AC87U router
* TP-Link SG105S 5-port gigabit switch

### **4.3: Find specifikationerne for enheden**

**Asus RT-AC87U router**

* **Modelnavn:** Asus RT-AC87U 5404640
* **Trådløs hastighed (Mbps):**	2301-3000 Mbps
* **Hastighed via kabel:** 10/100/1000
* **Wi-Fi generation:** Wi-Fi 5 (802.11ac)
* **Antal LAN-porte:** 6-8
* **Frekvensbåndsteknologi:** Dual-band
* **Netværkstilslutning:** Ethernet (LAN) ,  WAN ,  Wi-Fi
* **Sikkerhed:** WEP ,  WPS ,  WPA-PSK ,  WPA2-PSK ,  WPA Enterprise ,  WPA2 Enterprise

**TP-Link SG105S 5-port gigabit switch**

* **Produkttype:** Netværksswitch
* **Modelnavn:** TP-Link SD105S 5-port gigabit switch TL-SG105S
* **Antal LAN-porte:** 4-5
* **Netværkstilslutning:** Ethernet (LAN)
* **Trådløs teknologi:** Nej

### **4.4: Hvad bliver listet som begrænsninger? Synes du det er høje tal? lave tal?**
...



## **Opgave 5 - Internet speeds**

### **5.1 og 5.2: Gå på speedtest og find ud af hvad hastigheden er?**
**Første forsøg:**

Download: 11,34 mbps

Upload: 33,24 mbps

**Andet forsøg:**

Download: 6,37 mbps

Upload: 31,69 mbps

**Tredje forsøg:**

Download: 9,63 mbps

Upload: 29,11 mbps

**Fjerde forsøg**

Download: 4,12 mbps

Upload: 28,26 mbps

### **5.3: Best guess: hvad er bottleneck, og hvorfor**
Hvor der kommer flere requests/anmodninger end netværks enheden kan håndtere og der derfor opstår en flaskehals.



## **Opgave 11 - DNS**

### **Opgave 11.1: Gennemfør rummet https://tryhackme.com/room/dnsindetail**

Done

### **Opgave 11.2: Skriv dine opdagelser ned undervejs og dokumenter det på gitlab**

DNS serverne består af en række servere som fungere i et hieraki (beskrevet længere nede).

### **Besvar efterfølgende**

### **Opgave 11.1: Forklar med dine egne ord hvad DNS er og hvad det bruges til**

DNS oversætter domæne navne til IP-adresser.

Det betyder, at vi ikke behøver at huske IP-adressen på den hjemeside vi gerne vil besøge. Vi kan blot nøjes med at huske domænenavnet.

### **Opgave 11.2: Hvilke dele af CIA er aktuelle og hvordan kan DNS sikres? https://blog.cloudflare.com/dns-encryption-explained/**

*Tilgængelighed:* Hvis DNS serverne ikke virker, kan vi ikke tilgå nogen hjemmesider, da vi ikke kan IP-adresserne i hovedet.



### **Opgave 11.3: Hvilken port benytter DNS ?**

DNS benytter port 53.

### **Opgave 11.4: Beskriv DNS domæne hierakiet**

![Alt text](img/DNS_hieraki.png)

**Local cache:** Når man forespørge på et domæne navn, tjekker computeren først dens lokale cache for at se om hjemmesiden har været slået op for nylig. Har den ikke det, sender den en forespørgsel til den Recursive DNS server.

**Resolving/recursive DNS server:** Den bliver stillet til rådighed af ens ISP. Den har også en lokal cache over hjemmesider der for nylig har været slået op. Findes adressen her, sendes den tilbage til ens computer. Hvis hjemmesiden ikke kan findes her, sendes forespørgslen videre til internettets root DNS servers.

**Root DNS servers:** Root servernes opgave er at redirecte til den rigtige TLD server afhængig af forespørgslen. Er forespørgslen fx på en .com hjemmeside, vil root DNS serveren sende forespørgslen videre til den TLD server som håndtere .com adresser.

**TLD (Top Level Domain) server:** TLD serverne holder styr på hvor den authoritative server som kan besvarer DNS forespørgslen er. 

**Authoritative DNS server/Nameserver:** Er ansvarlig for at opbevarer DNS records for det pågældende domæne navn. Den returnere IP-adressen for det pågældende domæne tilbage til Recursive DNS serveren – som sender det tilbage til computeren.

### **Opgave 11.5: Forklar disse DNS records: A, AAAA, MX, TXT og CNAME**

DNS record types -> DNS er ikke kun for hjemmesider, der findes også andre typer af DNS.

**A Record:** Håndtere IPv4 adresser

**AAAA Record:** Håndtere IPv6 adresser

**CNAME Record:** Håndtere andre domæne navne for den samme hjemmesider, og som derfor også har en anden IP-adresse??

**MX Record:** 

**TXT Record:** kan opbevarer tekst baseret data. 


### **Opgave 11.6: Brug "nslookup" til at undersøge hvor mange mailservere ucl.dk har**

UCL har 3 mailservere.

### **Opgave 11.7: Brug "dig" til at finde txt records for ucl.dk**

.

### **Opgave 11.8: Brug wireshark til:**

#### **Opgave 11.8.A: Filtrere DNS trafik. Dokumenter hvilket filter du skal bruge for kun at se DNS trafik**

For kun at se DNS trafik bruges følgende filter:

"udp.port==53"

#### **Opgave 11.8.B: Lav screenshots af eksempler på DNS trafik**

.

## **Opgave 12 - DNS**

### **Opgave 12.1: Gennemfør THM rummet: [https://tryhackme.com/room/passiverecon](https://tryhackme.com/room/passiverecon)**

.

### **Opgave 12.2: Læs om og registrer en konto på securitytrails.com**

.

### **Opgave 12.3: Beskriv forskellen på aktiv og passiv rekognoscering**

**Passiv rekognoscering**

Her indsamler man kun viden om målet via offentlig tilgængelig viden.

Det er viden som man kan indsamle fra offentlige ressourcer uden direkte at interagere med målet.

Det kan fx være ved at:
•	Slå DNS records op for et domæne fra en offentlig DNS server.
•	Tjekke jobannoncer relateret til målet
•	Læse nyhedsartikler omkring virksomheden/målet

**Aktiv rekognoscering**

Her interagere man aktivt med målet.

Eksempler på aktiv rekognoscering:
•	Connecte til en af virksomhedens servere som fx HTTP, FTP og SMTP.
•	Ringe til virksomheden i forsøget på at få informationer (social engineering)
•	Gå ind i virksomhedens lokaler ved at udgive sig for fx at være reparatør


### **Opgave 12.4: Lav en liste med de værktøjer du kender til aktiv rekognoscering, beskriv kort deres formål**

* Wireshark
* Nmap

### **Opgave 12.5: Lav en liste med de værktøjer du kender til passiv rekognoscering, beskriv kort deres formål**

.

### **Opgave 12.6: Beskriv med dine egne ord hvad en fjendtlig aktør kan bruge rekognoscering til**

Indsamle viden om den virksomhed som de gerne vil angribe. 

Herunder indsamle viden om angrebsflader.

### **Opgave 12.7: Beskriv med dine egne ord hvad du kan bruge rekognoscering til, når du skal arbejde med sikkerhed**

Indsamle viden om virksomhedens agrebsfalder, så man ved hvilke angrebsfalder man skal beskytte.

### ** Opgave 12.8: Brug kun PASSIV rekognoscering til at undersøge ucl.dk. Lav en lille rappport med de ting du har fundet ud af og hvordan en angriber evt. kan misbruge oplysningerne. Rapporten skal du IKKE offentliggøres på gitlab, kun på din egen computer, resultater kan du fortælle om til næste undervisning!**


## **Opgave 22 - Routing**

### **Information**
"Routing" er den mekanisme på lag 3 som håndterer at sende pakker på tværs af subnets.

Denne opgave er om at læse routing tables.

### **Instruktioner**

Start Kali

Check at der er internet adgang

Kør ip a og forklar hvad der ses

Kør ip route og forkalr hvad der ses

Tegn er netværksdiagram med de relevante enheder og ip adresser

Gør der samme på hosten.
På windows ses ip adresser vha. ipconfig og routing informationer med route print

### **Besvarelse**

**2.1: Start Kali og check at der er internet adgang**


**2.2: Kør ip a og forklar hvad der ses**

![Alt text](img/Kali_ip_a.png)

Der ses bl.a.:

lo = loadback - som bruges til localhost

eth0 0 = ethernet

* IP-adresse for enheden
* Broadcast IP-adresse
* MAC-adresse for enheden: 00:0c:29:c4:e6:dd og for broadcast: ff:ff:ff:ff:ff:ff

**2.3: Kør ip route og forkalr hvad der ses**

![Alt text](img/kali_ip_route.png)

Der ses enhedens routing tabel.

Den indeholder her:
* Routerens IP-adresse: 192.168.146.2
* Enhedens egen IP-adresse: 192.168.146.128
* Netværkets IP-adresse: 192.168.146.0/24
* Metric -> metric er en prioriterings liste

**2.4: Tegn er netværksdiagram med de relevante enheder og ip adresser**

![Alt text](img/Opgave_22_netv%C3%A6rksdiagram.png)

(Mangler DHCP og DNS?)

**2.5: Gør der samme på hosten - på windows ses ip adresser vha. ipconfig og routing informationer med route print**


## **Opgave 23 - Dynamic routing**
### **Information**

"Routing" er den mekanisme på lag 3 som håndterer at sende pakker på tværs af subnets.

Dynamic routing er når man opdaterer routing tabellen løbende med info udefra. Vi vil ikke dykke ned i detaljerne.

OSPF er en protokol til internt brug
BGP er protokollen der bruges på internettet

### **Instruktioner**
Forklar hurtigt hvorfor dynamisk routing er essentiel for internettet

Lav et netværkseksempel der illustrerer hvorfor vi ønsker at opdatere routing tabeller
inkludér hvadder er i de enkelte enheders routing tabeler

Bonus: Hvordan passer dette med CIA?

### **Besvarelse**

**Forklar hurtigt hvorfor dynamisk routing er essentiel for internettet**

![Alt text](img/dynamic_routing.png)

Dynamisk routing handler om at en router kender alle sine veje til at nå en bestemt anden router.

Skal router 1 sende en pakke til router 5, men f.eks. forbindelsen mellem router 3 og 5 er nede, så kender router 1 de andre veje den kan tage til router 5 - altså via enten router 2 eller 4

**Lav et netværkseksempel der illustrerer hvorfor vi ønsker at opdatere routing tabeller** 

**Inkludér hvad der er i de enkelte enheders routing tabeler**

**Hvordan passer dette med CIA?**

Det hjælper med at opretholde tilgængeligheden/availability.


## Opgave 30 - Certifikater
### **Information**

Certifikater er et af fundamenterne i det moderne web.

### **Instruktioner**

1. Gå til https://dr.dk

2. Klik på hængelåsen ved siden af url'en, og vis certifikat information

3. Hvem har udstedt certifikatet, hvem er det udstedt til og hvad er levetiden?

Certificate chains er relevant her

1. Gå til https://ucl.dk

2. Klik på hængelåsen ved siden af url'en, og vis certifikat information

3. Hvem har udstedt certifikatet, hvem er det udstedt til og hvad er levetiden?

4. Start en kali

5. Åben en terminal og kør openssl s_client ucl.dk:443. 
Brug ctrl+c til at stoppe kommandoen

6. Sammenlign informationen fra browseren

### **Besvarelse**

1. **Gå til https://dr.dk**

2. **Klik på hængelåsen ved siden af url'en, og vis certifikat information**

3. **Hvem har udstedt certifikatet, hvem er det udstedt til og hvad er levetiden? (Certificate chains er relevant her)**

Udstedt til: www.dr.dk

Udstedt af: ISRG Root X1 --> R3 - Let's Encrypt

Levetid: onsdag d. 5. april 2023 - tirsdag d. 4 juli 2023

1. **Gå til https://ucl.dk**

2. **Klik på hængelåsen ved siden af url'en, og vis certifikat information**

3. **Hvem har udstedt certifikatet, hvem er det udstedt til og hvad er levetiden?**

Udstedt til: sni.cloudflaressl.com - Cloudflare, Inc.

Udstedt af: Baltimore Cyber Trust Root --> Cloudflare Inc ECC CA-3 - Cloudflare, Inc.

Levetid: fredag d. 12. august 2022 - lørdag d. 12 august 2023

4. **Start en kali**

5. **Åben en terminal og kør openssl s_client ucl.dk:443. Brug ctrl+c til at stoppe kommandoen**

![Alt text](img/Certifikat_del_1.png)

![Alt text](img/Certifikat_del_2.png)

6. **Sammenlign informationen fra browseren**
