# ntopng

Bruges til monitorering af netværk.

## Installation af ntopng på en xubuntu maskine

Gå til: 
[https://www.ntop.org/products/traffic-analysis/ntop/](https://www.ntop.org/products/traffic-analysis/ntop/ "ntopng")

Scoll ned til "Get it" et stykke nede på siden - og tryk download.

Der åbnes et ny side - tryk her på "software repository"

Vælg det rigtige OS -> på xubuntu "linux"

Der åbnes et nyt (lille) vindue -> tryk på nr. 2 under Ubunutu = apt-stable.ntop.org to access stable builds packages using the APT tool.

I terminalen på xubuntu maskinen indtast de kommandoer som står under "Ubuntu -> 22.04 LTS" - de er følgende:
![Alt text](img/ntopng_installations_kommandoer.png)