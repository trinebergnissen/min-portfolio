# Linux

## **Linux kommandoer**
echo "tekst" -> printer teksten til skærmen

whoami -> fortæller hvem man er logget på som

ls -> listning -> giver en liste af af filer i den pågældende mappe

cd "navn på mappe/folder" -> navigere til den pågældende mappe

cat "navn på fil" -> skriver outputet fra filen

pwd -> viser stien/path'en

touch -> opretter en fil

mkdir -> opretter en ny mappe

cp -> copier en fil eller en mappe

mv -> flyt en fil eller en mappe

rm -> sletter en fil eller en mappe

file -> finder filtypen

cd /var/log -> for at tilgå log filerne

nano "filnavn" -> opretter en ny fil med det angivne filnavn
-> bruges også til at åbne og skrive i en fil
(for at komme ud af filen tryk "ctrl + x" og derefter "ctrl + y" hvis ændringerne skal gemmes og til sidst tryk "enter")


"filnavn" | jq -> viser en json fil formateret pænere end hvis den blot åbnes med cat


DF og DU -> viser hvor meget plads der er opbrugt í filsystemet

cat /etc/*release -> viser distributions info - hvilken type linux man bruger, hvilken version osv.

uname -a -> viser hvilken version af linux kernen man bruger

.

root (root bruger) -> er en superbruger/administrator (admin) -> den bruger kan alt i systemet

sudo (sudo bruger) -> hvis en bruger er i sudo gruppen (kaldet ??), kan brugeren bruge sudo kommandoen -> bruges sudo foran ens kommandoen, kan brugeren alt det som superbrugeren kan >(hvis de er i sudo gruppen) = de kan alt i systemet
Skriver man sudo foran sin kommando, eksekveres den altså som superbruger.

Sudo kommandoen er fx nødvendig for at installere nyt software eller ændre netværks indstillinger eller andet som påvirker hele systemet.

.

id –> Display user identity

chmod –> Change a file's mode

umask –> Set the default file permissions

su –> Run a shell as another user

sudo –> Execute a command as another user

chown –> Change a file's owner

chgrp –> Change a file's group ownership

passwd –> Change a user's password

.

"program navn" -v - fx ssldump -v -> viser hvilken version af propgrammet man har - fejler den har man ikke programmet installeret

-help -> viser hvilke funktioner der er

man -> viser en manual

.

sudo passwd root -> kommando til at ændre adgangskoden for root brugeren

.

## **Rettigheder i Linux**

Der findes tre typer af rettigheder:

r = read

w = write

x = execute 

Kan fx se sådan ud: -rw-rw-r

Det første er brugerens rettigheder - i dette tilfælde rw = read og write

Det næste er rettighederne for den gruppe som brugeren er i - i dette tilfælde rw = read og write

Det sidste er alle andres rettigheder - i dette tilfælde r = read


## **Linux taster**
" = shift + Ø

- = +

_ = "shift" + "+"

{ = shift + Å

} = shift + ^