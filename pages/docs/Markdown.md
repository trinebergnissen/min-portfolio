# Markdown

### **Headers**
"#" = h1

"##" = h2

"###" = h3

"####" = h4

"#####" = h5

"######" = h6

### **Fed tekst**
"stjerne stjerne" tekst "stjerne stjerne" (uden situationstegn)

Eksempel: **tekst**

### **Skrå tekst**
"stjerne" tekst "stjerne" (uden situationstegn)

Eksempel: *tekst*

### **Lav en ny side**
Opret en fil i mappen "docs".
Filenavnet skal slutte med ".md" for at siden vises.

### **Indæt et billede**
Lig billedet ind i mappen "img" - via stifinder på computeren

Drag and drop derefter billedet ind på den markdown side hvor det skal være 
(husk: hold shift nede inden man giver slip på billedet, der hvor det skal placeres på siden)

**Eller**

Hvis billedet ligger i samme mappe som den side man vil indsætte det på - skriv:

![Alt text](img/Inds%C3%A6t_billede_1.png)

Og så vælge fra dropdownen, som kommer frem.

Hvis billeder ligger i en anden mappe - skriv:

![Alt text](img/Inds%C3%A6t_billede_2.png)

Og så vælge mappen fra dropdownen, som kommer frem.

### **Indsæt link**
![Alt text](img/Inds%C3%A6t_link.png)

