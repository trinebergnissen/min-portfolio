# Python cheat sheet

## **Kommentarer**
"#" foran sætning gør linjen til en kommentar.

Eksempel: "# Dette er en kommentar"

## **Udskriv til consolen**
print("tekst") -> udskriver "tekst" til consolen.

Eksempel: print("Hello World")

## **Datatyper**
![Alt text](img/Datatyper_i_python.png)

## **Operatorer**
![Alt text](img/Operator_i_python.png)

## **If og elif og else sætninger**
if

elif

else:

elif = else if -> i python bruges elif i stedet for else if (som kendes fra C# og Java).

Eksempel:

![Alt text](img/if_elif_else_statements.png)

Lig desuden mærke til at både if sætningen, elif sætningen og else sætningen slutter med ":". I if sætningen og elif sætningen står det i slutningen af linjen - det samme gør sig gældende med else sætningen, men her står der ikke andet end else og ":" kommer derfor lige efter else

-> kolon bruges i python til at markere at if sætningen, elif eller else sætningen slutter

## **Funktioner**
En funktion -> minder om en metode i Java og C#.

![Alt text](img/funktioner_i_python.png)

En funktioner starter med ordet "def".

Herefter navnet på funktionen - i dette tilfælde "sayHello".

Efterfulgt af () - som kan indeholde input værdier - i dette tilfælde "name".

**At kalde en funktion**

For at kalde funktionen skrives funktionens navn "sayHello" efterfuldt af () - som indeholder den input værdi som metoden skal kaldes med - i dette tilfælde "ben".

## **"Read and write" i filer**

Man kan læse (read) og skrive (write) i filer med python.

Eksempel på read/læse:

![Alt text](img/L%C3%A6s_fra_en_fil_i_python.png)

Eksempel på write/skrive:

![Alt text](img/Skriv_i_filer_i_python.png)

## **Libraries/biblioteker**
Libraries kan hentes ind i python via keywordet "import".

![Alt text](img/Libraries_i_python.png)