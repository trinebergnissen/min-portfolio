# Wireshark

## Filtre

For kun at se DNS trafik bruges følgende filter: "udp.port==53"

For at filtrer på en ip-adresse: "ip.addr==xxx.xxx.xxx.xxx"

## Datastrøm

Når vi kigger på en data strøm, er der forskellige parametre.

Connection kan være "keep alive" -> det betyder at efter at vi har lavet handshaket, så beholder vi forbindelsen åben. Har vi brug for at forespørge på mere, skal vi altså ikke til at lave handshake igen.

## Output - OSI modellen

Outputtet fra wireshark når vi kigger på en pakke, følger OSI modellen.

Der er 5 punkter vi kan folde ud:

Frame -> Fysiske lag

Ethernet -> Data Link laget -> her kan vi derfor bl.a. se MAC-adresser

Internet Protocol Version... -> Netværks laget -> her kan vi derfor bl.a. se IP-adresser

User Diagram Protocol... -> Transport laget

Domain Name System... -> Applikations laget (det dækker her over alle de tre øverste lag i OSI (Session, Præsentation, Applikation))
