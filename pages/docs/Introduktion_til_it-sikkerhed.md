# Introduktion til it-sikkerhed

## Connect til TryHackMe netværk via VPN
1. Åben Kali Linux virtuel maskine.
2. Gå til https://tryhackme.com/access?o=vpn i en browser på den virtuelle maskine.
3. Ret VPN server i toppen af siden til ...EU-Regular-1??
4. Tryk på "OpenVPN"
5. Følg guiden

    5.1. Tryk på Download

    5.2. Kør kommandoen: "sudo apt install openvpn" i kommandoprom

    5.3. Gå til Downloads mappen ved at køre kommandoen: "cd Downloads"

    5.4. Åben den downloadet fil ved at kører kommandoen: "sudo openvpn "navn på filen"" - fx "sudo openvpn TrineNissen.ovpn"

6. Opdater siden i browseren - punktet "connected" i toppen af siden, skal nu skifte fra at have et kryds til at have et flueben


## Linux kommandoer
echo "tekst" -> printer teksten til skærmen

whoami -> fortæller hvem man er logget på som

ls -> listning -> giver en liste af af filer i den pågældende mappe

cd "navn på mappe/folder" -> navigere til den pågældende mappe

cat "navn på fil" -> skriver outputet fra filen

pwd -> viser stien/path'en

touch -> opretter en fil

mkdir -> opretter en ny mappe

cp -> copier en fil eller en mappe

mv -> flyt en fil eller en mappe

rm -> sletter en fil eller en mappe

file -> finder filtypen