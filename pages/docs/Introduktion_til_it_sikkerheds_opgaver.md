# Introduktions til it-sikkerheds opgaver

## Opgave 28
### **Vælg et scenarie**
Privat person data på computer og cloud

### **Vurder, prioitér scenariet i forhold til CIA modellen**

**C**: At andre ikke kan tilgå ens personlige ting - kan være forskelligt fra person til person hvad det dækker over

**I**: At der ikke bliver ændret i ens ting, så man ikke længere kan stole på dem

**A**: Man kan af forskellige årsager have behov for at tilgå ting på sin computer

### **Hvilke(n) type hacker angreb vil være mest aktuelt at tage forholdsregler imod? (DDos, ransomware, etc.)**

**Ransomware**: I forhold til ens egen computer -> kan bryde **tilgængeligheden** vis de kryptere filer eller låser computeren imod en løsesum

**DDOS**: I forhold til cloud -> kan bryde **tilgængeligheden**.

**Phishing**: Kan ligge til grund for efterfølgende angreb fx ransomware.

Men kan også stjæle data -> og derved bryde **fortroligheden**.

### **Hvilke forholdsregler kan i tage for at opretholde CIA i jeres scenarie (kryptering, adgangskontrol, hashing, logging etc.)**
Backup -> beskytter **integritet** og **tilgængelighed**

Kryptering -> beskytter **fortrolighed** og **integritet**

Stærke passwords -> beskytter **fortrolighed**


## **Opgave 12**

Kør "socket_1.py" med default url, analyser med wireshark og besvar følgende:

**Hvad er destinations ip?**

192.241.136.170

**Hvilke protokoller benyttes?**

TCP og HTTP

**Hvilken content-type bruges i http header ?**

text /plain -> plain text

**Er data krypteret ?**

Nej - men hjemmesiden er også en HTTP

**Hvilken iso standard handler dokumentet om?**


## **Opgave 24 - VPN**

### **Information**

VPN er krypterede tunneller. 

Vi vil bruge wireguard til at illustrere hvordan det virker.

### **Instruktioner**

Start 2x kali på det samme subnet

Ping 8.8.8.8 på begge og genfind trafik i wireshark

Tegn et netværksdiagram med relevant enheder

Set up wg client på den ene and server på den anden

Der er en guide her

Brug 172.16.100.1/24 til serveren og 172.16.100.2/32 til klienten

Tilføj en route på klienten sudo ip route add 8.8.8.0/8 via 172.16.100.1

Ping 172.16.100.1 fra klienten og genfind trafik

Ping 8.8.8.8 fra client og genfind trafik.

Virker tunnelen?

Updater netværksdiagram med nye ip adresser og interfaces

Se på routing tables og foklar hvad der ses

### **Besvarelse**

**Start 2x kali på det samme subnet**

Kali 1/serveren: 172.16.100.1/24

Kali 2/klienten: 172.16.100.2/32

**Ping 8.8.8.8 på begge og genfind trafik i wireshark**

**Tegn et netværksdiagram med relevant enheder**

Startet to kali maskiner og lavet netværksdiagram:

![Alt text](img/Netv%C3%A6rk_opgave_24_netv%C3%A6rksdiagram_kali_ip.png)

Tilpasset med de IP-adresser som er angiver i opgaven:

![Alt text](img/Netv%C3%A6rk_opgave_24_netv%C3%A6rksdiagram_angivet_ip.png)

**Set up wg client på den ene and server på den anden**

**Der er en guide her**

**Brug 172.16.100.1/24 til serveren og 172.16.100.2/32 til klienten**

**Tilføj en route på klienten sudo ip route add 8.8.8.0/8 via 172.16.100.1**

**Ping 172.16.100.1 fra klienten og genfind trafik**

**Ping 8.8.8.8 fra client og genfind trafik**

**Virker tunnelen?**

**Updater netværksdiagram med nye ip adresser og interfaces**

**Se på routing tables og foklar hvad der ses**
