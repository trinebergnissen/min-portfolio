# Xubuntu maskiner og vsrx router i vmware

**Hvad er opsat i dette netværk?**

* 2 xubuntu maskiner
    * Ubuntu client (1)
    * Ubuntu client (2)
* vsrx router
    * For login tast root + "adgangskode"


Ubuntu client 1 -> koble op på et "vmnet1"

Ubuntu client 2 -> koble op på et andet "vmnet2"

vsrx router -> koble op på samme netværk som client 1 (vmnet1) + koble op på samme netværk som client 2 (vmnet2) + koblet op på NAT (vmnet8).

Derudover skal netværkene (vmnet1, vmnet2 og vmnet 8) sættes rigtigt op i netværks indstillingerne i vmware.

Se evt. denne video for at se hvordan det hele er sat op i vmware:
[https://www.youtube.com/watch?v=SlPj1QYHzlM](https://www.youtube.com/watch?v=SlPj1QYHzlM "Video om opsætningen af enhederne i vmware")

**Derudover skal IP-adressen sættes manuelt på ubuntu maskinerne**

1) Tryk på netværkssymbolet på ubuntu maskinen i øverste højre hjørne

2) Tryk på "Edit connection"

3) Tryk på "wired connection" og så på tandhjulet i bunden

4) Tryk på "IPv4 connection"

5) Ændre method til "manuel"

6) Tryk på "add"

7) Tilføj IP-adressen + netmasken + gateway (IP-adressen til routeren)

**Herefter skulle xubuntu maskinerne gerne kunne pinge både routeren og hinanden**