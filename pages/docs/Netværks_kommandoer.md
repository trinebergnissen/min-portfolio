# Netværks kommandoer i cmd

**ping "ip-adresse"** -> pinger den angivende IP-adresse. Man kan fx se om man kan få forbindelse til en anden enhed ved at pinge den.

**ipconfig** -> viser bl.a. enhedens IP-adresse

**tracert "domain"** - fx: "tracert dr.dk" -> viser de routere som pakken rammer på vejen inden den rammer dr.dk - samt hvor lang tid det tog før den ramte hver router